Title: Syncthing update includes breaking protocol changes
Author: Kylie McClain <somasis@exherbo.org>
Content-Type: text/plain
Posted: 2015-04-21
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: net-p2p/syncthing[<0.11.0]

Syncthing's v0.11.0 update introduces backwards-incompatible changes
to the REST interface, and as a result is not compatible with any of
the previous versions. This includes clients such as syncthing-gtk,
and other peers you sync with.

So, before upgrading, keep in mind that you will need to update any nodes
which use an older Syncthing version, or you will lose ability to sync
with them!

Check the changelog and relase notes, they're a lot of fun:
    https://github.com/syncthing/syncthing/releases/tag/v0.11.0
    https://forum.syncthing.net/t/syncthing-v0-11-0-release-notes/2426
