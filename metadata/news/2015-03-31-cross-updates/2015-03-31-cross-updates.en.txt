Title: Information about cross branch
Author: Kylie McClain <somasis@exherbo.org>
Content-Type: text/plain
Posted: 2015-03-31
Revision: 1
News-Item-Format: 1.0

Just a heads up that until cross is merged, I don't plan on updating
the non-cross stuff as vigilantly as I have before, mostly because I
prefer to test all the commits I do on my machine (which is now cross
only) before pushing anywhere.

I will, of course accept any and all patches that update things in
this repository if anyone sends them in.

I highly encourage all users to start developing on cross so that the
migration is a lot less painful for others.

