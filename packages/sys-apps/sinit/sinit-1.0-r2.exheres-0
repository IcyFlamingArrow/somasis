# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

HOMEPAGE="http://git.suckless.org/${PN}"
DOWNLOADS="
    ${HOMEPAGE}/snapshot/${PNV}.tar.gz
"

SUMMARY="A suckless init daemon that doesn't complicate with runlevels and inittabs"
SLOT="0"
LICENCES="MIT"

PLATFORMS="~amd64 ~armv7"

DEPENDENCIES=""

MYOPTIONS=""

DEFAULT_SRC_COMPILE_PARAMS=(
    CC=$(exhost --tool-prefix)cc
    LD=$(exhost --tool-prefix)ld
)

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=/usr/$(exhost --target)
    MANPREFIX=/usr/share/man
)

src_prepare() {
    edo sed -e '/^PREFIX/d'                 \
            -e '/^MANPREFIX/d'              \
            -e '/^CC =/d'                   \
            -e '/^LD =/d'                   \
            -e 's/CFLAGS.*=/CFLAGS+= /'     \
            -e 's/LDFLAGS.*=/LDFLAGS+= /'   \
            -i config.mk
}

src_configure() {
    rc_init='/usr/host/bin/rc.init'
    rc_reboot='/usr/host/bin/rc.shutdown'
    rc_reboot_args='reboot'
    rc_poweroff='/usr/host/bin/rc.shutdown'
    rc_poweroff_args='poweroff'

    edo sed -e "s:\"/bin/rc.init\":\"${rc_init}\":" \
            -e "s:\"/bin/rc.shutdown\", \"reboot\":\"${rc_reboot}\", \"${rc_reboot_args}\":"        \
            -e "s:\"/bin/rc.shutdown\", \"poweroff\":\"${rc_poweroff}\", \"${rc_poweroff_args}\":"  \
            -i config.def.h
    edo sed -e "s:/bin/rc.init:${rc_init}:" \
            -e "s:/bin/rc.shutdown poweroff:${rc_poweroff} ${rc_poweroff_args}:"    \
            -e "s:/bin/rc.shutdown reboot:${rc_reboot} ${rc_reboot_args}:"          \
            -i sinit.8
}

src_install() {
    default
    dobin "${FILES}"/reboot 
    dobin "${FILES}"/shutdown
    dobin "${FILES}"/poweroff

    alternatives_for    init ${PN} 10  \
                        /usr/$(exhost --target)/bin/init        sinit           \
                        /usr/$(exhost --target)/bin/reboot      ${PN}.reboot    \
                        /usr/$(exhost --target)/bin/shutdown    ${PN}.shutdown  \
                        /usr/$(exhost --target)/bin/poweroff    ${PN}.poweroff  \
                        /usr/share/man/man8/init.8              sinit.8
}

pkg_postinst() {
    alternatives_pkg_postinst
    einfo "sinit runs '/usr/host/bin/rc.init' at system start, runs"
    einfo "'/usr/host/bin/rc.shutdown reboot' at reboot, and"
    einfo "'/usr/host/bin/rc.shutdown poweroff' at shutdown."
    einfo "Do not set sinit as the init provider unless you make these files."
}

